/** User     :双日新都市開発株式会社
 *  System   :オプション受注管理
 *  App      :物件別建物マスタ
 *  Function :画面制御
 *  Create   :2018.02.05 by fukui
 *  **********************************************************
 *  Update   :2018.4.4 by oota 
 *           :2018.4.6 by oota 入金リスト取得のロジック変更
 *  **********************************************************
 * 物件マスタ       GET PUT
 * 顧客マスタ       GET PUT
 * (双)受注管理     GET PUT
 * (業)受注管理     GET PUT
 */

//--------------------------------------
// 広域変数宣言
//--------------------------------------
var QUOTE_APP=39;//協力会社様向け受注管理アプリ
var NYUKIN_APP=40;//入金管理アプリ
jQuery.noConflict();
(function($) {
    "use strict";

	//------------------------------------------------------
    // 定数宣言
	//------------------------------------------------------
var dbg_flg = 1;
//var dbg_flg = 0;
	const EVENT_INDEX_SHOW = ['app.record.index.show'];
	const EVENT_DETAIL_SHOW = ['app.record.detail.show'];
	
	const EVENT_INDEX_EDIT_SHOW = ['app.record.index.edit.show'];
	const EVENT_CREATE_EDIT_SHOW = ['app.record.create.show','app.record.edit.show'];
	
	const EVENT_INDEX_EDIT_SUBMIT = ['app.record.index.edit.submit'];
	const EVENT_CREATE_EDIT_SUBMIT = ['app.record.create.submit','app.record.edit.submit'];

	const EVENT_INDEX_EDIT_SUCCESS = ['app.record.index.edit.submit.success'];
	const EVENT_CREATE_EDIT_SUCCESS = ['app.record.create.submit.success','app.record.edit.submit.success'];

	const EVENT_INDEX_DELETE_SUBMIT = ['app.record.index.delete.submit'];
	const EVENT_DETAIL_DELETE_SUBMIT = ['app.record.detail.delete.submit'];
	
	const EVENT_PROCESS_PROCEED = ['app.record.detail.process.proceed'];

	/*
	const EVENT_CHANGE_XXXX = [
		'app.record.create.change.<フィールドコード>','app.record.edit.change.<フィールドコード>'
	];
	 */
	 
	//------------------------------------------------------
	// 外部変数宣言
	//------------------------------------------------------
  	//------------------------------------------------------
	// 関連アプリ更新
	//  function : updapp_build
	//  @event   : 画面表示情報
	//  @appid   : 更新対象アプリID
	//  return   : promise
	//------------------------------------------------------
	function updapp_build(event, appid){
		//--------------------------------------
		// 更新条件生成
		//--------------------------------------
		var txt_build_id = event['record']['txt_build_id']['value'];
		var str_query = '';
		
		var guest_flg = false;
		
		switch(appid){
		case kintone.app.getRelatedRecordsTargetAppId('rel_customer'): // 顧客マスタ
			str_query = 'lup_build_id=\"' + txt_build_id + '\"';
			break;
		case kintone.app.getRelatedRecordsTargetAppId('rel_m_order'): // 協力会社様向け受注管理
			str_query = 'lup_build_id=\"' + txt_build_id + '\"';
			break;
		case kintone.app.getRelatedRecordsTargetAppId('rel_d_order'): // 業者様向け受注管理
			str_query = 'lup_build_id=\"' + txt_build_id + '\"';
			break;
		}

		//--------------------------------------
		// 関連アプリ情報取得
		//--------------------------------------
		var get_param = {
			app: appid,
			query: str_query,
			fields: ['$id', 'lup_build_id'],
			totalCount: true,
			isGuest: guest_flg
		};
		
		return kintoneUtility.rest.getAllRecordsByQuery(get_param).then(function(get_resp) {
			// success
			
			// 更新情報生成
			var recs = get_resp.records;
			var recs_cnt = recs.length;
			var records = [];

			// 件数チェック
			if(recs_cnt === 0){
				return ;
			}
			
			for(var i = 0; i < recs_cnt; i++){
				var rec = recs[i];
				var wk_rec = {};
				var wk_buf = {};
				wk_rec['id'] = rec['$id']['value'];
				wk_buf['lup_build_id'] = {value:rec['lup_build_id']['value']}; 
				wk_rec['record'] = wk_buf;
				records.push(wk_rec);
			}
			
			var put_param = {
				app: appid,
				records: records,
				isGuest: guest_flg
			};
		
			//--------------------------------------
			// 関連アプリ情報更新
			//--------------------------------------
			kintoneUtility.rest.putAllRecords(put_param).then(function(put_resp) {
				// success
if(dbg_flg)console.log(put_resp);
			}).catch(function(put_error) {
				// error
				console.log(put_error);
			});
		}).catch(function(get_error) {
			// error
			console.log(get_error);
		});		
	}

	/* function  : フィールド表示/非表示
	 * @show_flg : true/表示 false/非表示
	 * return    : void
	 */
	function set_fieldshown(show_flg){
        kintone.app.record.setFieldShown('grp_admin', show_flg);
	}

    /*
     * function  : 建物CD採番
     * @event    : 画面情報
     * return    : promise
     */
    function set_build_code(event){
        var record = event.record;
        var condo_cd = record['lup_condo_cd']['value'];
        
        // 物件CD未設定
        if(!condo_cd){
            return ; // 終了
        }
        
        // 採番情報取得
        var get_field = ['txt_build_cd'];
        var get_query = 'lup_condo_cd = \"' + condo_cd + '\"';
        get_query += ' order by txt_build_cd limit 1';
        
        var get_body = {
            'app' : kintone.app.getId(),
            'query' : get_query,
            'field' : get_field
        };
        
        return kintone.api(kintone.api.url('/k/v1/records', true), 'GET', get_body).then(function(get_resp) {
            var get_list = get_resp['records'];
            var get_len = get_list.length;
            var new_build_cd = '01';
            
            if(get_len !== 0){
                var wk_build_cd = Number(get_list[0]['txt_build_cd']['value']);
                wk_build_cd++;
                new_build_cd = ('00' + wk_build_cd).slice(-2);
            }
            // 建物CD設定
            record['txt_build_cd']['value'] = new_build_cd;
        }, function(get_error) {
            console.log(get_error);
            alert('建物CDの採番採番に失敗しました。\n[' + get_error.message + ']');
        });    
    }
    
	/* function  : 関連レコードチェック
	 * @event    : 画面情報
	 * return    : promise
	 */
	function chk_relate(event){
		return kintone.Promise(function(resolve, reject) {
			resolve(event);
		});
	}
	/*function        :同一顧客の入金リストを取得
	 *@customerId     :顧客IDの配列
	 *@kubun          :内訳区分
	 *return          :生成した入金リスト
	 */
	 function getNyukinList(customerId,kubun){
	 	var body={
			app:NYUKIN_APP,
			query:'lup_customer_id in (' + customerId.join(",") + ')'
		}
		return kintoneUtility.rest.getAllRecordsByQuery(body);
	 }
	/*function   :出力する入金CSVファイルを生成する
	 *@event     :画面情報
	 *return     :生成したCSVファイル
	 */
	 function createCSVData(event){
	 	var record=event.record;
		var kubun=$('[name=kubun] option:selected').text();//選択されている内訳区分を取得
		var condo_cd=record["lup_condo_cd"]["value"];//物件CD
		var build_cd=record["txt_build_id"]["value"];//建物ID
		var csv="";//生成したCSVを格納する変数
		var getParam={
			app:QUOTE_APP,
			query:'txt_condo_cd = "' + condo_cd +'" and txt_build_id = "' + build_cd + '" and drp_breakdown in ("' + kubun + '")' 
		}
		kintoneUtility.rest.getAllRecordsByQuery(getParam).then(function(get_resp){
			var respRecords=get_resp.records;
			//取得件数が0件であれば、処理を終了
			if(respRecords.length===0){
				alert("該当の入金リストが存在しません");
				return null;
			}
			//CSVのヘッダー情報を登録
			csv="["+respRecords[0]["txt_condo_name"]["value"]+"("+respRecords[0]["txt_build_name"]["value"]+")"+kubun+"入金リスト]"+
				'\n部屋,タイプ,契約者名,税込合計（申込）,内消費税,OP券,請求額合計,入金額合計,残金合計,返金合計,完了確認,入金/返金日付,入金額/返金額\n';
			var customerIdArray=[];//入金リストを取得する顧客IDを格納するための配列
			for(var i=0;i<respRecords.length;i++){
				//検索する顧客IDを配列に格納
				var txt_customer_id=respRecords[i]["txt_customer_id"]["value"];//協力会社_受注_顧客ID参照用
				customerIdArray.push('"' + txt_customer_id + '"');
			}
			getNyukinList(customerIdArray).then(function(get_resp2){
				var nyukinList=get_resp2.records;
				for(i=0;i<respRecords.length;i++){
					var csvOrderInfo="";//協力会社向けの情報を格納
					var nyukinFlg=false;//同一顧客の入金リストが存在するかどうかを判断するフラグ
					var customerId=respRecords[i]["txt_customer_id"]["value"];//顧客ID_参照用
					var roomNumber=respRecords[i]["txt_room_number"]["value"];//住戸番号
					var type=respRecords[i]["txt_room_type"]["value"];//タイプ
					var customerName=respRecords[i]["txt_customer_name"]["value"];//顧客名
					var amount=respRecords[i]["num_include_tax"]["value"];//税込合計(申込)
					var tax=respRecords[i]["num_tax"]["value"];//内消費税(申込)
					var opTotal=respRecords[i]["num_op_total"]["value"];//OP券総額
					var demand=respRecords[i]["num_demand"]["value"];//請求金額
					var payment=respRecords[i]["num_payment"]["value"];//入金額
					var balance=respRecords[i]["num_balance"]["value"];//入金残額
					var commit=""//完了確認
					//完了確認にチェックが入っていたら値を入力
					if(respRecords[i]["chk_payment_commit"]["value"].length>0){
						commit=respRecords[i]["chk_payment_commit"]["value"][0];
					}
					var refund=0;//返金額
					//残額が0以下の場合、返金額に値を代入
					if(Number(balance)<0){
						refund=Number(balance)*-1;
					}
					//受注アプリと入金管理のリレーション
					for(var j=0;j<nyukinList.length;j++){
						var nyukinCustomerId=nyukinList[j]["lup_customer_id"]["value"];//入金管理_顧客ID
						//二行目以降はヘッダー情報を空白に置換
						if(j!==0){
							customerId="";
							roomNumber="";
							type="";
							customerName="";
							amount="";
							tax="";
							opTotal="";
							demand="";
							payment="";
							balance="";
							refund="";
							commit="";
						}
						//入金リスト以外の部分のCSVデータを作成
						csvOrderInfo=roomNumber+","+type+","+customerName+","+amount+","+tax+","+opTotal+","+demand+","+payment+","+
							balance+","+refund+','+commit;
						var nyukinTable=nyukinList[j]["tbl_detail"]["value"];//請求情報サブテーブル
						//入金管理と受注アプリで同一の顧客IDがあれば処理を実行
						if(nyukinCustomerId===customerId){
							nyukinFlg=true;//フラグを更新する
							//入金サブテーブルが一行もない場合
							if(nyukinTable.length===0){
								csv +=csvOrderInfo+','+""+','+""+'\r\n';
								break;
							}
							var receipt=nyukinList[j]["date_receipt"]["value"];//入金日付
							var sum=0;//入金金額のサマリーを格納
							for(var k=0;k<nyukinTable.length;k++){
								var respKubun=nyukinTable[k]["value"]["txt_breakdown"]["value"];//内訳区分
								//受注アプリと同一の内訳区分の入金額をサマリー
								if(respKubun===kubun){
									sum +=Number(nyukinTable[i]["value"]["num_detail_receipt"]["value"]);
								}
							}
							csv +=csvOrderInfo+',' + receipt + ',' + sum + '\r\n'; 
							break;
						}
					}
					//入金リストが存在しない場合
					if(nyukinFlg===false){
						csv+=roomNumber+","+type+","+customerName+","+amount+","+tax+","+opTotal+","+demand+","+payment+","+
							balance+","+refund+','+commit+','+""+','+""+'\r\n';
					}
				}
				exportCSV(csv);//CSVをローカルに生成する
			}).catch(function(get_error2){
				alert("入金リスト出力の処理に失敗しました");
				console.log(get_error2);
			});
		}).catch(function(get_error){
			alert("入金リスト出力の処理に失敗しました");
			console.log(get_error);
		})
	 }
	/*function   :内訳区分を選択するドロップダウンを生成する
	 *@event     :画面情報
	 *return     :avoid
	 */
	 function createKubunList(event){
	 	var button=document.createElement("button");
		button.id="export";
		button.innerHTML="入金リストを出力";
		button.className = 'kintoneplugin-button-normal';//プラグイン向けCSSを流用する場合
		//区分を選択するドロップダウンリストを生成
		var selectKubunHtml='<div class="kintoneplugin-select-outer"><div class="kintoneplugin-select"><div>'+
							'<select name="kubun">'+
								' <option>オプション会</option>'+
								' <option>追加オプション</option>'+
								' <option>Ni\'sセレクト</option>'+
								' <option>工事内(追加NIS)</option>'+
								' <option>設計変更/変更工事</option>'+
								' <option>リフォーム</option>'+
								' <option>モデル</option>'+
								' <option>その他</option>'+
    						'</select>'
 						'</div></div></div>';
		kintone.app.record.getSpaceElement('csvExport').innerHTML=selectKubunHtml;
		kintone.app.record.getSpaceElement('csvExport').appendChild(button);
		button.onclick=function(){
			createCSVData(event);//出力するCSVを生成
		}
	 }
	/*function    :作成したCSVファイルをローカルにダウンロードする
	 *@csv        :作成するCSVファイルの内容
	 *return      :avoid
	 */
	 function exportCSV(csv){
	 	 var bom  = new Uint8Array([0xEF, 0xBB, 0xBF]);
		 var blob = new Blob([bom,csv],{'type': 'text/csv'});
		 var url = window.URL || window.webkitURL;
		 var blobUrl = url.createObjectURL(blob);
		 var alink = document.createElement("a");
		 alink.innerHTML = 'ダウンロード';
		 alink.download = '入金リスト'+moment().format('YYYYMMDD')+".csv";
		 if (window.navigator.msSaveBlob) {
			 // for IE
			 window.navigator.msSaveBlob(blob, '入金リスト'+moment().format('YYYYMMDD')+".csv");
		 }
		 else if (window.URL && window.URL.createObjectURL) {
			 // for Firefox
			 alink.href = window.URL.createObjectURL(blob);
			 document.body.appendChild(alink);
			 alink.click();
			 document.body.removeChild(alink);
		 }
		 else if (window.webkitURL && window.webkitURL.createObject) {
			 // for Chrome
			 alink.href = window.webkitURL.createObjectURL(blob);
			 alink.click();
		 }
	 }
    //--------------------------------------
    // 詳細画面制御
    //--------------------------------------
	kintone.events.on(EVENT_DETAIL_SHOW, function(event) {
		//--------------------------------------
		// 表示/非表示
		//--------------------------------------
		set_fieldshown(false);
		createKubunList(event);
		return event;
		
	});
	
    //--------------------------------------
    // 登録/編集画面制御
    //--------------------------------------
	kintone.events.on(EVENT_CREATE_EDIT_SHOW, function(event) {
		//--------------------------------------
		// 変数宣言
		//--------------------------------------
		var record = event.record;
		
		//--------------------------------------
		// 初期値設定/複製対応
		//--------------------------------------
		if(event.type === 'app.record.create.show'){
    		record['txt_build_cd']['value'] = null; // 建物CD
    	    record['lup_condo_cd']['lookup'] = true;
		}
		
		//--------------------------------------
		// 表示/非表示
		//--------------------------------------
		set_fieldshown(false);
		
		//--------------------------------------
		// 編集可/不可
		//--------------------------------------
		record['txt_build_cd']['disabled'] = true; // 建物CD
		
		return event;
		
	});
	
    //--------------------------------------
    // 登録/編集保存時制御
    //--------------------------------------
	kintone.events.on(EVENT_CREATE_EDIT_SUBMIT, function(event) {
		//--------------------------------------
		// 変数宣言
		//--------------------------------------
		var record = event.record;
	    record['lup_condo_cd']['lookup'] = true;
		
		//--------------------------------------
		// 建物CD採番
		//--------------------------------------
		if(event.type === 'app.record.create.submit'){
		    // 同一物件CDで通番を採番
		    set_build_code(event);
		}
		
		return event;
		
	});
	
	//--------------------------------------
	// 保存完了時制御
	//--------------------------------------
	kintone.events.on(EVENT_CREATE_EDIT_SUCCESS, function(event) {
        updapp_build(event, kintone.app.getRelatedRecordsTargetAppId('rel_customer')); // 顧客マスタ
        updapp_build(event, kintone.app.getRelatedRecordsTargetAppId('rel_m_order')); // 協力会社様向け受注管理
        updapp_build(event, kintone.app.getRelatedRecordsTargetAppId('rel_d_order')); // 業者様向け受注管理

		return event;
	});

	//------------------------------------------------------
	// 一覧編集回避処理
	//------------------------------------------------------
	kintone.events.on(EVENT_INDEX_EDIT_SHOW, function(event) {
		//------------------------------
		// 変数宣言
		//------------------------------
		var record = event.record;
		
		//--------------------------
		// 一覧画面編集回避対応
		//--------------------------
		for(var str in record){
			if(event.record[str]){
				event.record[str]['disabled'] = true;
			}
		}
		
		return event;
	});

})(jQuery);
